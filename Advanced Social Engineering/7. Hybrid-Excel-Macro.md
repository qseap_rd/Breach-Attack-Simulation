When it comes to evading defenses, nothing stops attacker from being creative. Lets look at what an attacker can do when combing the normal macro based attack and VBA Stomping.

<h2> Walkthrough:- </h2>

1. First lets create a Excel workbook that supports Macros.

![](/Images/NEV-1.png)

2. Lets grab the previously created macro from Magic Unicorn and paste it in the macro section of the Excel workbook.

![](/Images/NEV-2.png)

3. Slight changes are needed that is in Excel we should use WorkBook_Open instead of DOcument_Open()

![](/Images/NEV-3.png)

4. Save the document and setup listener and re-open the file.  We will get a command execution.

![](/Images/NEV-4.png)

![](/Images/NEV-5.png)

5. But what about AV Evasion? Lets upload the document to virustotal and see the hits.

![](/Images/NEV-6.png)

![](/Images/NEV-7.png)

6. 34/62 hits, Symentac and Mcaffe detected it as malicious.

7. So now lets combine the EXCEL Macro + VBA Stomping.

8. Copy the .doc to kali and lets stamp this with a malicious one.

![](/Images/NEV-8.png)

9. Perfect it got reduced heavly from 34 to 11.

![](/Images/NEV-9.png)

![](/Images/NEV-10.png)

10. This type Symentac and Mcaffe said its innocent.

11. Lets run this and see whether its a fucntional one or not.

![](/Images/NEV-11.png)

12. After clicking enable content. We will get a shell.

![](/Images/NEV-12.png)

<h2> References:- </h2>
1. https://gitlab.com/qseap_rd/Breach-Attack-Simulation



