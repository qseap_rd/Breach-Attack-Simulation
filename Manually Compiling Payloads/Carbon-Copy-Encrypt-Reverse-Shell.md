To make av detection less, there are many tools that perform some kind of obfusctation, binary certificate spooofing etc. Tools like that is CarbonCopy is written by ParanoidNinja https://github.com/paranoidninja/CarbonCopy

<h2> Walkthrough:- </h2>

1.Grab a copy of CarbonCopy to kali instance.

![](/Images/CC1.png)

2. Follow the syntax below to spoof certificate and sign the executable with that.
```
python3 CarbonCopy.py www.qseap.com 443 bs-rs.exe signed-bs-rs.exe
```

Now lets check the detection rate for the signed executable.

![](/Images/CC4.png)

![](/Images/CC5.png)

30/71 not bad for a simple trick.

-But as said it bypasses Symentac and Mcaffe.

<h2> References:- <br/> </h2>
1. https://twitter.com/ninjaparanoid/status/1056080108786798592?lang=en <br/>
2. https://twitter.com/ninjaparanoid/status/1056080108786798592?lang=en <br/>
3. https://github.com/paranoidninja/CarbonCopy <br/>
