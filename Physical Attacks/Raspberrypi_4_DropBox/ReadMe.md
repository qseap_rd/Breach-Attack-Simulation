# Setup:-

- Download the kali rasp .img from kali official repo. Then plug the memory card into the card reader and then stick it to the PC. Lets erase the previous content that is present in the card.

![](/Images/rasp4-1.png)

![](/Images/rasp4-2.png)

![](/Images/rasp4-3.png)


![](/Images/rasp4-4.png)

- Now lets select kali image and flash it into the memory card.

![](/Images/rasp4-5.png)

![](/Images/rasp4-6.png)

![](/Images/rasp4-7.png)

![](/Images/rasp4-8.png)

After successfull flashing remove the device and replugin.
![](/Images/rasp4-9.png)

- Now mount the to the kali VM, so that we can make some minor changes to make raspberry pi headless.

![](/Images/rasp4-10.png)

![](/Images/rasp4-11.png)

![](/Images/rasp4-12.png)

![](/Images/rasp4-13.png)

![](/Images/rasp4-14.png)

![](/Images/rasp4-15.png)

![](/Images/rasp4-16.png)

- Generally mounted devies will be located at /media/. Browse to that and we will find the mounted volumes

![](/Images/rasp4-17.png)

- Now Navigate to BOOT, then open config.txt and uncomment the hdmi_force_hotplug line, and also uncomment the hdmi_group and hdmi_mode and set those values according, in this case they are 1 and 16.

![](/Images/rasp4-18.png)

- As we want our headless rasp to connect to wifi directly, we will manually write a wifi profile so that kali directly connects to wifi reading that file. Remember to write a connection profile first in the etc/NetworkManager/system-connections/<Name_of_the_wifi.nmconnection> as shown below. And also change [ifupdown] managed value to true in etc/NetworkManager/NetworkManager.conf.

![](/Images/rasp4-19.png)

- Change the configuration of interfaces file in etc/network/ as shown below. [ This makes wlan0 to be enabled and grab ip from dhcp ]

![](/Images/rasp4-20.png)

- Next to automatically get past bootloader we need to make changes to lightdm config file in etc/lightdm named as lightdm.conf

![](/Images/rasp4-21.png)

- Uncomment the pam-autologin-serivce as shown below. [ please observe the line number ie 49 as shown below before changing ]

![](/Images/rasp4-22.png)

- Next uncomment autologin-user and set it to kali and uncomment autologin-user-timeout line [ line number 126 and 127 respectively ]

![](/Images/rasp4-23.png)

- All the changes need to be made are made. So lets unmount the volumes and eject and plug them to raspberry pi.

![](/Images/rasp4-24.png)

![](/Images/rasp4-25.png)

- As we configured our kali to automatically use a wifi profile, it worked and connected to our Wifi named ALPHA as kali [ see below kali device is connected to our network, that kali is our raspberry pi device ]

![](/Images/rasp4-26.png)

- Lets login and do further setup. SSH into kali with credentials kali:kali

![](/Images/rasp4-27.png)

- Update and upgrade to get the bleeding edge tools in our kali. 

![](/Images/rasp4-28.png)

![](/Images/rasp4-29.png)

- And install dbus-x11 packege with apt install dbus-x11 as shown below [ this solves some error we encounter with x11 vnc server ]

![](/Images/rasp4-30.png)

- Next we will install vncserver to get a GUI. [ This is not needed but acts as a backup incase ssh fails ]. So we will use KaliRPIVNCSetup file from techchipnet github to automate the installation process

![](/Images/rasp4-31.png)

![](/Images/rasp4-32.png)

- After installation of vncserver in kali linux, install vnc viewwer in your host machine and login to the vncserver. [ remember the credentials of the vncserver that is set during installation ]

![](/Images/rasp4-33.png)

![](/Images/rasp4-34.png)

- Remember X11Vnc create a virutal sessions, so by default it uses :1 and then increments from there. So when connecting to it, make sure you type out the virutal session number too as shown below [ in our case virutal session is of :1, so we appended :1 to the vnc server IP.

![](/Images/rasp4-35.png)

# Setting up an Access Point using Hostapd.

- Check the ip of the wlan0.

![](/Images/rasp4-36.png)

- Lets set up an Access Point by using wlan0 and we will access rasp with ethernet connection that is sshing to 192.168.0.168. To make access point edit interfaces file in the /etc/network as shown below.

![](/Images/rasp4-37.png)

- Install hostapd

![](/Images/rasp4-38.png)

- When the package is downloaded, create the file /etc/hostapd/hostapd.conf and paste the below text. If you know the channel of the victim’s network, change the channel in the file to different, which is further to avoid the same frequency band. For example if the attacked network uses the 3’rd channel, set it to 11’th. Set SSID and wpa_passphrase as you want. SSID is the name of the network which you will connect from mobile phone and wpa_passphrase is the password to this network. I’ve set the name to AndroidAP to make it unsuspected (looks like default network name when using a phone as a Wi-Fi router):

![](/Images/rasp4-39.png)

- Set default config of hostapd by editing the file /etc/default/hostapd and set the DAEMON_CONF variable.

![](/Images/rasp4-41.png)

- Enable hostapd service to start it automatically when Raspberry is turning on (I had to unmask hostapd).


![](/Images/rasp4-43.png)

# Setting up dnsmasq:-

![](/Images/rasp4-46.png)

- Check whether the edited config file is of the correct syntax using dnsmasq --test and enable it to run automatically after boot as shown below.

![](/Images/rasp4-47.png)

# CONFIGURING MASQUERADING:-

- You’ll need to install a couple of convenience packages that allow you to retain your firewall configuration rules across reboots. To do this, run the following in an open terminal or SSH window:

![](/Images/rasp4-48.png)

- We’ll now enable IP forwarding to make our dropbox function as a router an connect different networks together. This instructs the Pi to forward all data packets in all directions, according to the routing and firewall settings. Edit the file /etc/sysctl.conf and uncomment the below line

![](/Images/rasp4-49.png)

- Apply the change using sysctl.

![](/Images/rasp4-50.png)

- Once this is set up, you’ll need to configure your firewall rules. This involves allowing the wireless interface to forward data back and forth, and the masquerading itself. To do this, type the following in an open terminal window or over SSH:

![](/Images/rasp4-51.png)

- You’re now safe to save your rules. Before you do, however, you should check that the firewall rules have been added correctly by typing the following in a terminal or SSH window

![](/Images/rasp4-52.png)

- If you’re ready to save, type the following in a terminal/SSH window

![](/Images/rasp4-53.png)

- After reboot, I found that the rules weren’t being saved. I tried creating a cron job with no luck so I created a custom systemd service to ensure routing is enabled on boot. Let’s go through the steps. First we need to create a basic bash script

![](/Images/rasp4-54.png)

![](/Images/rasp4-55.png)

![](/Images/rasp4-56.png)

![](/Images/rasp4-57.png)

![](/Images/rasp4-58.png)


# Setup Automatic Reverse SSH Tunnel


- This section assumes you have a command and control server accessible on the Internet and that server has SSH enabled on port 22. For my C2 server, I use Amazon’s Lightsail but you can any cloud service or even a server at your house with port forwarding enabled

- Install autossh and generate ssh-keys on raspberrypi4.

![](/Images/rasp4-59.png)

- For C2 i am qseap using qseap's running EC2 which has public ip 65.1.107.27

![](/Images/rasp4-60.png)

![](/Images/rasp4-61.png)

- Transfer the generated ssh public key using scp and put it in the c2 server authorized_keys, so that we can use key based auth and login automatically instead of password based.

![](/Images/rasp4-62.png)

![](/Images/rasp4-63.png)

![](/Images/rasp4-64.png)

- Now we can login automatically to C2 server we will automate this process and build a reverse ssh tunnel as soon as the raspberry pi is powered on to do that we will use autossh with some switches that prevents from getting our c2 server itself to be compromised.

![](/Images/rasp4-65.png)

- After we entered above command it should get connected to our c2 and should open up localport 6667 , so that our reverse ssh tunnel is successfull. we can check it using netstat -atnp command.

![](/Images/rasp4-66.png)

- Lets automate this reverse ssl tunnel process so that we will get connection to our c2 as soon as the device is turned on. We will create a bash script and a cronjob to execute it for every 5 mintues as shown below.

![](/Images/rasp4-67.png)

- Reboot the raspberry pi and after powering on we can see a ssh connection is made and port 6667 is open on our c2 server as shown below. We can ssh to the localport 6667 and can login into that raspberry pi device [ if you are not aware of tunneling, please give it a read ]

![](/Images/rasp4-68.png)

![](/Images/rasp4-69.png)

# Setup of OpenVPN Client and Server

- While we can do a reverse SSH shell to access our Raspberry Pi 4 device, we will also configure the device to use a reverse OpenVPN connection over port 443 (HTTPS). Since the Raspberry Pi 4 will be dropped on the back of a system (or switch) inside an organization, we won't be able to directly connect to it. Therefore, we will have the Raspberry Pi 4 first go outbound via port 443 for VPN back to our OpenVPN AS server. From our attacker Kali box, we will have to also log into the VPN server. Once the Raspberry Pi 4 and attacker machine are VPN'd into our OpenVPN C2 server, we can remote into the Raspberry Pi 4 to scan or exploit systems. This ensures that if the client blocks port 22 outbound for SSH, we have another option to connect over port 443 (HTTPS).

- There are three items we need to configure for this setup:

OpenVPN AS server on the Internet.
Raspberry Pi 4 device.
Attacker system.

- Setting Up a VPS OpenVPN AS Server
 Go to https://aws.amazon.com/lightsail/ and create a new VPS
- Once created, go to Manage -> Networking

- Add two Firewall TCP Ports (443 and 943)

- Install an OS like Ubuntu. Then make sure to chmod 600 your SSH keys and login to your VPS server from your attacker system

![](/Images/rasp4-70.png)

![](/Images/rasp4-71.png)

- We created a lightsail instance and that got assigned a ip of 13.233.204.120 and I downloaded the keys to get connected using ssh.

![](/Images/rasp4-72.png)

- Using SSH connect to the instance

![](/Images/rasp4-73.png)

- But as we want to have a public static ip we will create it. Click on Create Static IP and make sure you allow port 993 and port 443 through firewall.

![](/Images/rasp4-74.png)

![](/Images/rasp4-75.png)

![](/Images/rasp4-76.png)

- Finally we assigned a static ip to the instance that is 3.108.16.174

![](/Images/rasp4-78.png)

- Sometimes a new ssh key is given to us download it and connect to isntance using ssh.

![](/Images/rasp4-79.png)

- Next as we want to install openvpn-as we will follow the instructions in the below screenshots and install it.

![](/Images/rasp4-83.png)

![](/Images/rasp4-80.png)

![](/Images/rasp4-81.png)

![](/Images/rasp4-84.png)

- Instllation is completed, as shown below, we can able to access the admin panel. But before that we will clear the current profile and configure OpenVPN by using /usr/local/openvpn_as/bin/ovpn-init command.

![](/Images/rasp4-87.png)

- During the setup, a wizard will appear:

- Select the options accordingly and rest just type <Enter>

![](/Images/rasp4-90.png)

- Change the password of openvpn user.

![](/Images/rasp4-91.png)

- Next we will access the admin console of the Openvpn, remember to use public static ip to access it.

![](/Images/rasp4-92.png)

![](/Images/rasp4-93.png)

![](/Images/rasp4-94.png)

![](/Images/rasp4-95.png)

- Register into the OpenVPN site and get the activation key, openvps will allow 2 connections for free trial accounts.

![](/Images/rasp4-102.png)

![](/Images/rasp4-103.png)

![](/Images/rasp4-104.png)

- Next create 2 user profiles I named one as rasp4 for raspberry and one as qseapredteam. and make sure you Allow-AutoLogin to them

![](/Images/rasp4-97.png)

- And some changes are also to be made ie change the password for the accounts, and set Allow-Access From: all server-side private subnets and Allow-Access From: all other VPN Clients.

![](/Images/rasp4-100.png)

![](/Images/rasp4-101.png)

- Next when we generate openvpn profile files we will get a vpn access to a private ip of the EC2 instance, we have to change this to our public static IP.  To change refer screenshot below.

![](/Images/rasp4-107.png)

- Now go to https://[Your VPS]:943/?src=connect , For each user (rasp4 and redteam) login and download the profile - Yourself (autologin profile)

- Save as rasp4.ovpn and qseapredteam.ovpn accordingly.

- Setting Up the Raspberry Pi 4 and Initial Configuration:-

- Turn on the Raspberry Pi 4 and plug in your ethernet cable. Login to the Raspberry Pi 4 and install OpenVPN

- Configure OpenVPN to autostart the .conf files. Open /etc/default/openvpn and uncomment AUTOSTART=”all”.

![](/Images/rasp4-106.png)

- Copy your rasp4.ovpn file to the Raspberry Pi 4

![](/Images/rasp4-109.png)

- Lets check our VPN connections, there should be no device connected as we didn't initiate a vpn connection yet.

![](/Images/rasp4-110.png)

- Enable Openvpn to auto run after a boot everytime.

![](/Images/rasp4-111.png)

- Connecting after reboot. 

![](/Images/rasp4-113.png)

- Our raspberry pi will be connected to our VPN.

![](/Images/rasp4-114.png)

- Now in order to access it. We also needed to connect to it. we can do this from our windows machine which has openvpn connect application installed on it.

![](/Images/rasp4-115.png)


![](/Images/rasp4-116.png)

![](/Images/rasp4-117.png)

- Grab the donwloaded .ovpn profile of qseapreadteam and place it in the opevpn. It imports the profile.

![](/Images/rasp4-118.png)

- Connect to the exported profile.

![](/Images/rasp4-119.png)

- Refresh the OpenVPN Clients page, we can see we got connected to VPN.

![](/Images/rasp4-120.png)

- Now we can ssh to raspberry pi by using the VPN Address that is displayed in the OpenVPN clients page. Here in this case raspberry pi vpn address is 172.27.224.7.

![](/Images/rasp4-121.png)

