**Command execution with Rubber Ducky:-**

As we know rubber ducky is a keystoke injection tool. It just a acts as a keyboard and type out the contents we scripted when connected to pc.

-So here we will see how to craft a malicious payload that evades AV's and load DLL reflectively so that no AV scans that and we get a reverse shell.

To get started open visual studio, create a new project.

Select C# Console App template[.Net Framework]

![](/Images/ducky-1.png)

Name the project as CustomMal as that depends when we are loading the DLL reflectively.

![](/Images/ducky-2.png)

You will be preseneted with a most generic template for the console application.

![](/Images/ducky-3.png)

As we are planning to evade AV's, so we will make use of Win32API's namely VirtualAlloc,CreateThread, WaitForSingleObject. Copy the definations as is.

![](/Images/ducky-4.png)

Now copy the remaing code [showed down below] into the console application . Remeber to geneate the shellcode for the payloads in sharp format and name that buffer as qseap. [ swap out the shellcode ]

![](/Images/ducky-5.png)

select the x64 arch 

![](/Images/ducky-7.png)

and build the project

![](/Images/ducky-9.png)

Next run the executable to check whether our attempt to use win32api's and running shellcode is successfull or not. But before running it make sure you setup a handler.

![](/Images/ducky-10.png)

Well its working.

![](/Images/ducky-11.png)

Lets submit this executable to virustotal and check the how many AV's caught this. 18/66 not bad.

![](/Images/ducky-12.png)

It looks like our shellcode has a known template signatures. So we will reverse the array and un reverse at run time. To reverse array we will use online site and to unreverse we will use Array.Reverse function.

![](/Images/ducky-13.png)

![](/Images/ducky-14.png)

![](/Images/ducky-15.png)

![](/Images/ducky-16.png)

Build the project and check whether its giving us reverse shell or not.

![](/Images/ducky-18.png)

It works.

![](/Images/ducky-19.png)

Lets see submitting this to AV. 7/69 nice improvement. Observe we bypassed Symentac and other major AV's. We can transfer this executable and run it without any problem but as our goal is to run this through Rubber Ducky we need to do some more things.

![](/Images/ducky-20.png)

![](/Images/ducky-21.png)

So the idea is to generate a managed DLL and load it reflectively from powershell using rubber ducky. So we will create a managed DLL. To do that right click on the project name in the solution explorer Add- > New Project -> Class Library.

![](/Images/ducky-22.png)

![](/Images/ducky-24.png)

Name that Project Name as QseapLibrary

![](/Images/ducky-25.png)

To run our managed dll we need to place a runner method in the class as shown below.

![](/Images/ducky-26.png)

Put the c# winapi declarations outside of the runner class. And put shellcode and other declarations in the runner class.

![](/Images/ducky-27.png)

Dont forgot to import System.Runtime.InterOpServices. Build the project

![](/Images/ducky-28.png)

To load the resulted DLL reflectively in memory, the following commands need to be executed in the powershell session.

![](/Images/ducky-35.png)

After putting that command in powershell [ dont forget to setup a handler ]. We will get a reverse shell as shown below.

![](/Images/ducky-38.png)

![](/Images/ducky-36.png)

**Scripting the rubber ducky**

The following script automated typing the above mentioned commands that loads the DLL directly into memory.

![](/Images/ducky-39.png)

Using java we will convert this .txt to .bin that is acceptable and understood by the rubber ducky.

![](/Images/ducky-40.png)

Copy the inject.bin into sdcard of the rubber ducky. It automatically opens and types the commands. [ windows defender is turned on ]

![](/Images/ducky-43.png)

It grabs the QseapLibrary.dll from our kali webserver

![](/Images/ducky-42.png)

In order to make things cool I used InitialAutoRunScript option of handler to automatically to explorer.exe

![](/Images/ducky-41.png)

As the time of testing we evaded microsoft defender

![](/Images/ducky-45.png)


**Note:-**

1. All the C# code used here were pushed to repository. For the ConsoleApp code refer payload-exe.txt file and for the Class Library code refer to payload-dll.txt
