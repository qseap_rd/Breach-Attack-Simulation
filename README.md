# Breach Attack Simulation

<h2> This Document is entirely confidential and is owned by Qseap InfoTech. </h2>

The methods described in the repository is collected through intensive research and have been used by APT Groups. Any kind of usage of the described methods without any prior permissions on systems is strictly restricted. 

<h3> Points to be noted:- </h3>

1. Symmentac Instance is not present on our side, so we used Online AV Scanner i.e VirusTotal to scan our malware.
2. Even though we have a Mcaffe End Point, its not working perfectly, so there is some kind of switching to get the job done by scanning it through VirusTotal.

<h3> Points to Qseap Consultants:- </h3>

1. I used a most generic C# based reverse shell in most of the scenarios, but feel free to use your own Agents, Grunts, Implants.
2. If you switch to any specific C2 framework letme know so that I can build an executable to bypass defenses.
3. To get the most out of the social engineering attacks, be creative and combine techinques like General Macro attack + VBA Stomping and make it a Hybrid attack
4. Most of the Social Engineering needs to be done only after getting proper understanding of the victims. By far the methods in this repository are industry standards collected by going throgh various security talks. So try to be creative by implenting some kind of legit look to the attack [ discussed in Pretexting.md ]
5. And most of the initial access is done through certutil, feel free to use your own favourite intial code exec method.




