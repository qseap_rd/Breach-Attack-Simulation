Basically all the publicly availabe open source offensive tools are easily detected by the AV and signataures are made. To evade them we performed string replacement and compiled the C# tools and wrapped them in the PowerShell script . We did load them reflectively using [System.Relfection.Assembly].

- I Cloned the C# repositories of the tools
- Next made the access modifiers public, so that we can use load that reflectively.
- Compile the each individual binary
- GZip compress and base64 encode the compiled binary base64 and load it in powershell via [System.Reflection.Assemly]::LoadFile($Binary)


Tools we did this for.

1.BetterSafetyKatz



2.Rubeus



3.SafetyKatz



4.SeatBelt


5.SharpHound




6.SharpKatz



7.SharpUp



8.SharpView


9.WinPeas


Note:
-At the time of the creation of these obfuscated script, only say like 3 to 4 antivirus engines caught these script as malicious because they are using API hooking. So check once before using them.
