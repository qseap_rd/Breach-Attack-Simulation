
# Various logical attacks on ATM software are as follows.

**Man-in Middle attack**

The man-in middle attacks focus on the communication between the host and the ATM PC. The man-in-middle malware can fake the host response for the transactions without debiting the money from the account. The man-in middle malware is remotely installed within the network or at the highest software layer of the ATM PC.

**Data Sniffing Attacks**

In data sniffing attacks, the malware can only operate on specific operating systems. The malware installed on the ATM records the magnetic stripe information of the card. The cyber-criminals then use this information to conduct illegal activities.

**Skimming with Spoofing**

The cyber-criminals combine two ATM cyber-attacks, namely skimming and spoofing, to conduct the ATM attacks. They gain user information using the skimming attacks, and the later makes the spoof phone calls to the users and collect other essential data.

**Measures to identify logical attacks**

Unexpected system reboots during the middle of the transactions can turn out to be a malware attack.

•  A significant gap in the audit logs, where the transaction history record is missing, is a hint of logical attacks.

•  If the cash dispenser of ATM is out of cash unexpectedly within a short amount of time, it is another indication.

•  Loss of communications with ATM security system and irregular recordings in the CCTV footages are the sign of illegal activity.

**Physical attacks on ATM**

Physical attacks on ATMs are considered risky, as it not only leads to financial losses but also involves the risk to property and life. The physical attack involves solid and gas explosives attacks, along with physical removal of ATM from the site and later using other techniques to gain access to the cash dispenser.

**Shimming**
One may refer to shimming as an upgraded form of skimming. While it still targets cards, its focus is recording or stealing sensitive data from their embedded chips.

A paper-thin shimming device is inserted in the ATM’s card slot, where it sits between the card and the ATM’s chip reader. This way, the shimmer records data from the card chip while the machine’s chip reader is reading it. Unlike earlier skimming devices, shimmers can be virtually invisible if inserted perfectly, making them difficult to detect. However, one sign that an ATM could have a shimming device installed is a tight slot when you insert your bank card.

Data stolen from chip cards (also known as EMV cards) can be converted to magnetic stripe data, which in turn can be used to create fake-out versions of our traditional magnetic stripe cards.

**Black box attacks**
A black box is an electronic device—either another computer, mobile phone, tablet, or even a modified circuit board linked to a USB wire—that issues ATM commands at the fraudster’s bidding. The act of physically disconnecting the cash dispenser from the ATM computer to connect the black box bypasses the need for attackers to use a card or get authorization to confirm transactions. Off-premise retail ATMs are likely targets of this attack.

Malware-based attacks. As the name suggests, this kind of attack can use several different types of malware, including Ploutus, Anunak/Carbanak, Cutlet Maker, and SUCEFUL, which we’ll profile below. How they end up on the ATM’s computer or on its network is a matter we should all familiarize ourselves with.

# CheatSheets and Resources :-

https://www.coursehero.com/file/41535096/Atm-Hackingpdf/<br>
https://www.ptsecurity.com/ww-en/analytics/atm-vulnerabilities-2018/ [ * ]<br>
https://resources.infosecinstitute.com/topic/atm-penetration-testing<br>
https://gcsec.org/wp-content/uploads/2016/09/atm-security-publication.pdf<br>
https://medium.com/@ramimuleys/7-ways-to-hack-atms-without-violence-c46ad038b3ef [ * ]<br>
https://techcrunch.com/2020/08/06/hackers-atm-spit-cash/<br>
https://cloudsek.com/dark-web-and-atm-hacking/<br>
https://www.scribd.com/document/435352867/1-5118618482933497912-pdf<br>
https://owasp.org/www-chapter-london/assets/slides/OWASPLondon_20180125Leigh_Anne_Galloway_TYunusov_Buy_Hack_ATM.pdf<br>
https://evi1cg.me/archives/AppLocker_Bypass_Techniques.html<br>
https://cansecwest.com/slides/2016/CSW2016_Freingruber_Bypassing_Application_Whitelisting.pdf<br>
https://viralmaniar.github.io/atm%20hacking/kiosk%20hacking/shortcuts/<br>
https://www.ptsecurity.com/ww-en/analytics/atm-vulnerabilities-2018/ [ * ]<br>
https://resources.infosecinstitute.com/topic/atm-penetration-testing/<br>
https://gcsec.org/wp-content/uploads/2016/09/atm-security-publication.pdf<br>
https://medium.com/@ramimuleys/7-ways-to-hack-atms-without-violence-c46ad038b3ef [ * ]<br>
https://techcrunch.com/2020/08/06/hackers-atm-spit-cash/<br>
https://cloudsek.com/dark-web-and-atm-hacking/<br>
https://www.scribd.com/document/435352867/1-5118618482933497912-pdf<br>
https://owasp.org/www-chapter-london/assets/slides/OWASPLondon_20180125Leigh_Anne_Galloway_TYunusov_Buy_Hack_ATM.pdf<br>
https://evi1cg.me/archives/AppLocker_Bypass_Techniques.html<br>
https://cansecwest.com/slides/2016/CSW2016_Freingruber_Bypassing_Application_Whitelisting.pdf<br>
